import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
 
  { 
    path: 'jurisdiccion', 
    loadChildren: () => import('./jurisdiccion/jurisdiccion.module').then(m => m.JurisdiccionModule)
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
