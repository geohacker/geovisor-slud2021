import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { MapComponent } from './components/map/map.component';

import { GeoportalRoutingModule} from './geoportal-routing.module';
import { MaterialModule } from './../share/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    MapComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    GeoportalRoutingModule,
    LeafletModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class JurisdiccionModule { }
