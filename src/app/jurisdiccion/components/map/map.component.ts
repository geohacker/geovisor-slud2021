import { Component, AfterViewInit, Input, SimpleChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { latLng, Map, tileLayer} from 'leaflet';
import * as L from 'leaflet';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})

export class MapComponent  {
    // Instanciar el Mapa
    map: Map;

    layerImportShp = L.geoJSON();

  constructor(private http: HttpClient) { 
  }

   //Base Layer - Open Street Map definitions
   LAYER_OSM = {
    id: 'openstreetmap',
    name: 'Open Street Map',
    enabled: false,
    layer: tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      attribution: 'Open Street Map'
    })
  };

  options = {
    layers: this.LAYER_OSM.layer,
    zoom: 10,
    center: latLng(4.7, -74)
  };

  
  onMapReady(map: Map) {
    this.map = map;

    this.http.get('./assets/geojson/map.geojson').subscribe((json: any) => {
      L.geoJSON(json).addTo(map);
  });

  }

}