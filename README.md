# Requisitos: 

1. Node:  ``node -v``
2. npm: ``npm -v``
3. Angular CLI: ``npm install -g @angular/cli``

## Step by Step
- Crear Proyecto Angular
    ``ng new slud-geovisor``
- Ingresamos al proyecto
    ``cd ngx-leaflet-tutorial-ngcli``
- Agregamos las dependencias de Leaflet
    - $ ``npm install leaflet@1``
    - $ ``npm install @types/leaflet@1``
    - $ ``npm install @asymmetrik/ngx-leaflet@7``
- Una vez las dependencias son instaladas, agregamos el ngx-leaflet en el modulo principal del proyecto **src/app/app.module.ts**

```
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

...
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    LeafletModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

```

- Para renderizar correctamente el mapa, es necesario agregar los estilos en el archivo **angular.json**

```
{
  ...
  "styles": [
    "src/styles.css",
      "./node_modules/leaflet/dist/leaflet.scss"
    ],
  ...
}
```
- Crear el Mapa ...

# Geoportal

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
